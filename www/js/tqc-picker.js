angular.module('tqc-picker', [])

	.directive('picker', function($ionicPopup) 
	{
		return {
			restrict: 'E',
			replace: false,
			scope: {
				data: '=data'
			},
			link: function($scope, $element, $attrs) 
			{
				$scope.max = $scope.data.options.length - 1;
				$scope.i = $scope.data.set;
				$scope.picker = $scope.data.options[$scope.i];

				$element.html('<input ng-model="picker" type="text" value="'+$scope.picker+'"/>');
				
		        $scope.min = function()
		        {
		            var current = $scope.i;
		            console.debug($scope.picker);

		            if(current == 0)
		            {
		                $scope.i = 0;
		                $scope.picker = $scope.data.options[current];
		            } else
		            {
		                $scope.i = current - 1;
		                $scope.picker = $scope.data.options[$scope.i];
		            }
		        }

		        $scope.plus = function()
		        {
		            var current = $scope.i;
		            console.debug($scope.picker);

		            if(current == $scope.max)
		            {
		                $scope.i = $scope.max;
		                $scope.picker = $scope.data.options[current];
		            } else
		            {
		                $scope.i = current + 1;
		                $scope.picker = $scope.data.options[$scope.i];
		            }
		        }

				$element.on('click', function()
				{
					$ionicPopup.show({
              			template: '<div class="row"><div class="col col-33" align="right"><button class="button button-positive" ng-click="min()"><i class="icon ion-chevron-left"></i></button></div><div class="col col-33" align="center"><input ng-model="picker" class="input-picker" type="text" /></div><div class="col col-33" align="left"><button class="button button-positive" ng-click="plus()"><i class="icon ion-chevron-right"></i></button></div></div>',
              			title: $scope.data.title,
              			subTitle: '',
              			scope: $scope,
              			buttons: [
                		{
                  			text: 'Set',
                  			type: 'button-balanced',
                  			onTap: function () 
                  			{
                  				// Save changes
                  				$element.html('<input ng-model="picker" type="text" value="'+$scope.picker+'"/>');
                  			}
                		},
                		{
                  			text: 'Cancel',
                  			type: 'button-light',
                  			onTap: function () 
                  			{
                    			// Discard changes
                  			}
               			}]
            		});
				});
			}
		};
	});
	