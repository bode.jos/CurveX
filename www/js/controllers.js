angular.module('CurveX.controllers', [])

    .controller('AppController', function($scope, $rootScope) {
        //  Instellingen om te testen
        $rootScope.language = 'en';
        $rootScope.selectedPaint = 'red';
        $rootScope.intervals = [1,2,3,4,5,10,15,20,30,50];
        $rootScope.interval = 4;        
        $rootScope.timeouts = [5,10,15,30,60,90,120];
        $rootScope.timeout = 4;
        $rootScope.runtime = 0;
        $rootScope.block = 1;
        $rootScope.unit = 'c';
        $rootScope.triggerTemp = true;
        $rootScope.triggerTime = false;
    })

    .controller('DashboardController', function($scope, $ionicPopup) {
        $scope.links = [
            { name: 'Run', url: 'app.run'},
            { name: 'Run Setup', url: 'app.setup'},
            { name: 'Recordings', url: 'app.recordings'},
            { name: 'Instrument Setup', url: 'app.settings'}
        ];

        $scope.standby = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'System standy',
                template: 'Are you sure you want to put the system in standby?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        };
    })

    .controller('StartTimeController', function($scope, $state, $ionicModal) {
        $scope.modal = $ionicModal.fromTemplate('<ion-modal-view><ion-header-bar class="bar-assertive bar bar-header"><h1 class="title">Time Trigger</h1></ion-header-bar><ion-content class="has-header has-footer padding" style="margin-top: 50px" align="center"><h1>Starting in</h1><h2>00:34:19</h2></ion-content><div class="bar bar-footer bar-assertive"><div class="title" ng-click="abort()">Cancel</div></div></ion-modal-view>', {
            scope: $scope,
            animation: 'scale-in'
        })

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        $scope.$on('$ionicView.beforeEnter', function() {
           $scope.modal.show();
        });
    })

    .controller('StartController', function($scope, $state, $ionicPopup, $location, $rootScope, $ionicModal) {
        $scope.abort = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Stop recording?',
                template: '',
                cancelText: 'No',
                okText: 'Yes'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                    $location.path('/app/recordings/2');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.record = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: '',
                template: 'Paint type: Red<br>Interval: 3 seconds',
                cancelText: 'Cancel',
                okText: 'Start'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                    $state.go('app.record');
                } else {
                    console.log('You are not sure');
                }
            });
        };

        $scope.recordTemp = function() {
            var myPopup = $ionicPopup.show({
                template: 'Start at 40&deg;C<br>Stop at 30&deg;C',
                title: '',
                scope: $scope,
                buttons: [
                    { 
                        text: 'Stop',
                        type: 'button-light',
                        onTap: function() {
                            // Close
                        }
                    },
                    { 
                        text: 'Setup',
                        type: 'button-dark',
                        onTap: function() {
                            $state.go('app.trigger-temp');
                        }
                    },
                    {
                        text: 'Start',
                        type: 'button-positive',
                        onTap: function() {
                            $state.go('app.record-temp');
                        }
                    }
                ]
            });
        };

        $scope.recordTime = function() {
            var myPopup = $ionicPopup.show({
                template: 'Start on 1-4-2016 at 06:00 <br>Stop on 1-4-2016 at 07:45',
                title: '',
                scope: $scope,
                buttons: [
                    { 
                        text: 'Stop',
                        type: 'button-light',
                        onTap: function() {
                            // Close
                        }
                    },
                    { 
                        text: 'Setup',
                        type: 'button-dark',
                        onTap: function() {
                            $state.go('app.trigger-time');
                        }
                    },
                    {
                        text: 'Start',
                        type: 'button-positive',
                        onTap: function() {
                            $state.go('app.record-time');
                        }
                    }
                ]
            });
        };

        /*$scope.recordTemp = function() 
        {
            var confirmPopup = $ionicPopup.confirm({
                title: '',
                template: 'Start at 40&deg;C<br>Stop at 30&deg;C',
                cancelText: 'Cancel',
                okText: 'Start'
            });

            confirmPopup.then(function(res) 
            {
                if(res) 
                {
                    console.log('You are sure');
                    $state.go('app.record-temp');
                } else {
                    console.log('You are not sure');
                }
            });
        };*/
        /*
        $scope.recordTime = function() 
        {

            var confirmPopup = $ionicPopup.confirm({
                title: '',
                template: 'Start on 1-4-2016 at 06:00 <br>Stop on 1-4-2016 at 07:45 ',
                cancelText: 'Cancel',
                okText: 'Start'
            });

            confirmPopup.then(function(res) 
            {
                if(res)
                {
                    console.log('You are sure');
                    $state.go('app.record-time');
                } else {
                    console.log('You are not sure');
                }
            });

            $scope.recording = false;
        };
        */

        $scope.noProbes = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Warning',
                template: 'No probes connected.'
            });

            alertPopup.then(function(res) {
                console.log('No probes connected');
                // Niet naar /record
            });
        };

        $scope.runtime = $rootScope.runtime; 
        $scope.paint = $rootScope.selectedPaint; 
    })

    .controller('EditPaintController', function($scope, $state, $stateParams, $rootScope, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    // Save 
                    $state.go('app.paints');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.deletePaint = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Delete paint',
                template: 'Are you sure?',
                buttons: [
                    { 
                        text: 'No',
                        type: 'button-light'
                    },
                    {
                        text: 'Yes',
                        type: 'button-balanced',
                        onTap: function(e) {
                            // Delete
                        }
                    }
                ]
            });
        }

        $scope.item = {
            id: $stateParams.id,
            name: 'Red',
        }
        // Waardes moeten nog aangepast worden.
        $scope.cure = 
        {
            first: {
                temp: { 
                    title: 'Temperature',
                    set: 3,
                    options: [5,10,15,30,60,90,120]
                },
                time: {
                    title: 'Time',
                    set: 3,
                    options: [5,10,15,30,60,90,120]
                }
            },
            second: {
                temp: { 
                    title: 'Temperature',
                    set: 3,
                    options: [5,10,15,30,60,90,120]
                },
                time: {
                    title: 'Time',
                    set: 3,
                    options: [5,10,15,30,60,90,120]
                }
            },
            third: {
                temp: { 
                    title: 'Temperature',
                    set: 3,
                    options: [5,10,15,30,60,90,120]
                },
                time: {
                    title: 'Time',
                    set: 3,
                    options: [5,10,15,30,60,90,120]
                }
            }
        };
    })

    .controller('CreatePaintController', function($scope, $state, $stateParams, $rootScope, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    // Save
                    $state.go('app.paints');
                } else {
                    console.log('You are not sure');
                }
            });
        }
        // Waardes moeten nog aangepast worden.
        $scope.cure = {
            first: {
                temp: { 
                    title: 'Temperature',
                    set: 0,
                    options: [5,10,15,30,60,90,120]
                },
                time: {
                    title: 'Time',
                    set: 0,
                    options: [5,10,15,30,60,90,120]
                }
            },
            second: {
                temp: { 
                    title: 'Temperature',
                    set: 0,
                    options: [5,10,15,30,60,90,120]
                },
                time: {
                    title: 'Time',
                    set: 0,
                    options: [5,10,15,30,60,90,120]
                }
            },
            third: {
                temp: { 
                    title: 'Temperature',
                    set: 0,
                    options: [5,10,15,30,60,90,120]
                },
                time: {
                    title: 'Time',
                    set: 0,
                    options: [5,10,15,30,60,90,120]
                }
            }
        };
    })

    .controller('RealtimeController', function($scope, $state, $ionicPopup) {
    })

    .controller('RecordingsController', function($scope, $state, $ionicPopup) {
    })

    .controller('RecordingsViewController', function($scope, $state, $stateParams, $ionicPopup) {
        $scope.item = {
            id: $stateParams.id,
            paint: 'Orange',
            timestamp_start: 1461149626549,
            timestamp_stop: 1461153226549,
            records: 120,
            interval: 5,
            duration: 3600 // timestamp_start - timestart_stop
        }
     })

    .controller('ViewGraphController', function($scope, $state, $stateParams, $ionicPopup) {
    })

    .controller('IntervalController', function($scope, $state, $ionicPopup, $rootScope) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.i);
                    $rootScope.interval = $scope.i;
                    $state.go('app.setup');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.interval = {
            title: 'Interval seconds',
            set: 5,
            options: [1,2,3,4,5,10,15,20,30,50]
        };

        $scope.int = $scope.interval.options[$scope.interval.set];

        $scope.remaining = function($seconds) {
            var seconds = Math.floor(($seconds / 10) % 60);
            var minutes = Math.floor((($seconds / (60)) % 60));
            var hours = Math.floor((($seconds / (3600)) % 24));
            var days = Math.floor((($seconds / (3600)) / 24));

            return days + "d " + hours + "h " + minutes + "m";
        }
        // Voorbeeld
        $scope.runtime = $scope.remaining((3600*$scope.int));
        $rootScope.runtime = $scope.remaining((3600*$scope.int))
    })

    .controller('LanguageController', function($scope, $state, $ionicPopup, $rootScope) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.language);
                    $rootScope.language = $scope.language;
                    $state.go('app.settings');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.selectItem = function(val) {
            $scope.language = val;
            console.debug(val);
        }

        $scope.languages = [
            { name: 'English', val: 'en'},
            { name: 'Deutsch', val: 'de'},
            { name: 'Francios', val: 'fr'},
            { name: 'Espanol', val: 'es'},
            { name: 'Italiano', val: 'it'}
        ];
        $scope.language = $rootScope.language;
    })

    .controller('DisplayController', function($scope, $state, $ionicPopup, $rootScope) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.i);
                    $rootScope.timeout = $scope.i;
                    $state.go('app.settings'); 
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.display = {
            title: 'Interval seconds',
            set: 3,
            options: [5,10,15,30,60,90,120]
        };
    })

    .controller('SettingsController', function($scope, $state, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                    $state.go('app.settings');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.backButtonn = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                    $state.go('app.setup');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.settings = [
            { name: 'Language', url: 'app.language'},
            { name: 'Units', url: 'app.units'},
            { name: 'Date', url: 'app.date'},
            { name: 'Time', url: 'app.time'},
            { name: 'Display Timeout', url: 'app.display'},
            { name: 'System', url: 'app.system'}
        ];

        $scope.setup = [
            { name: 'Paints', url: 'app.paints'},
            { name: 'Triggers', url: 'app.triggers'},
            { name: 'Interval', url: 'app.interval'},
            { name: 'Log Blocks', url: 'app.blocks'}
        ];        

        $scope.paints = [
            { name: 'Blue', val: 'blue'},
            { name: 'Green', val: 'green'},
            { name: 'Orange', val: 'orange'},
            { name: 'Yellow', val: 'yellow'}
        ];
        $scope.paint = "blue"; 
    })

    .controller('DateTimeController', function($scope, $state, $rootScope, $ionicPopup, ionicDatePicker, ionicTimePicker, $filter) {
        $scope.backButton = function(route) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.trigger);
                    $rootScope.triggerTemp = $scope.temp;
                    $rootScope.triggerTime = $scope.time;
                    $state.go(route);
                } else {
                    console.log('You are not sure');
                }
            });
        }

        var a = new Date().getTime();
        $scope.selectedDate = $filter('date')(a, 'dd/MM/yyyy');

        var ipObj1 = {
          callback: function (val) {  //Mandatory
            console.log('Return value from the datepicker popup is : ' + val, new Date(val));
            var a = new Date(val);
            $scope.selectedDate = $filter('date')(a, 'dd/MM/yyyy');
          },
          from: new Date(2012, 1, 1), //Optional
          to: new Date(2016, 10, 30), //Optional
          inputDate: new Date(),      //Optional
          mondayFirst: true,          //Optional
          closeLabel: 'Cancel',
          closeOnSelect: false,       //Optional
          templateType: 'popup'       //Optional
        };

        $scope.openDatePicker = function() {
          ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.selectedTime = '11:13';

        $scope.setTime = function () {
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) === 'undefined') {
                        console.log('Time not selected');
                    } else {
                        var selectedTime = new Date(val * 1000);
                        $scope.selectedTime = selectedTime.getUTCHours() + ':' + selectedTime.getUTCMinutes();
                        console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                    }
                },
                inputTime: ((new Date()).getHours() * 60 * 60 + (new Date()).getMinutes() * 60),
                format: 24
            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        $scope.dateFormat = 'a';
        $scope.timeFormat = 'a';

        $scope.minutes = {
            title: 'Minutes',
            set: 0,
            options: [1,2,3,4,5,6,7,8,9,10]
        };
        $scope.hour = {
            title: 'Hour',
            set: 0,
            options: [1,2,3,4,5,6,7,8,9,10]
        };
        $scope.day = {
            title: 'Day',
            set: 0,
            options: [1,2,3,4,5,6,7,8,9,10]
        };
        $scope.month = {
            title: 'Month',
            set: 3,
            options: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        };
        $scope.year = {
            title: 'Year',
            set: 0,
            options: [2016, 2017, 2018, 2019, 2020]
        };
    })

    .controller('TriggerTempController', function($scope, $state, $rootScope, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.trigger);
                    $rootScope.triggerTemp = $scope.temp;
                    $state.go('app.triggers');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.temp = {
            start: {
                title: 'Start Temperature',
                set: 3,
                options: [5,10,15,30,60,90,120]
            },
            stop: {
                title: 'Stop Temperature',
                set: 3,
                options: [5,10,15,30,60,90,120]
            }
        };
    })

    .controller('TriggerTimeController', function($scope, $state, $rootScope, $ionicPopup, ionicTimePicker, ionicDatePicker, $filter) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.trigger);
                    $rootScope.triggerTime = $scope.time;
                    $state.go('app.triggers');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        var a = new Date().getTime();
        $scope.selectedDate = $filter('date')(a, 'dd/MM/yyyy');
        $scope.selectedDate2 = $filter('date')(a, 'dd/MM/yyyy');

        var ipObj1 = {
          callback: function (val) {  //Mandatory
            console.log('Return value from the datepicker popup is : ' + val, new Date(val));
            var a = new Date(val);
            $scope.selectedDate = $filter('date')(a, 'dd/MM/yyyy');
          },
          from: new Date(2012, 1, 1), //Optional
          to: new Date(2016, 10, 30), //Optional
          inputDate: new Date(),      //Optional
          mondayFirst: true,          //Optional
          closeOnSelect: false,       //Optional
          templateType: 'popup'       //Optional
        };

        $scope.openDatePicker = function() {
          ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.selectedTime = '08:00';
        $scope.selectedTime2 = '10:00';

        $scope.setTime = function () {
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) === 'undefined') {
                        console.log('Time not selected');
                    } else {
                        var selectedTime = new Date(val * 1000);
                        $scope.selectedTime = selectedTime.getUTCHours() + ':' + selectedTime.getUTCMinutes();
                        console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                    }
                },
                inputTime: ((new Date()).getHours() * 60 * 60 + (new Date()).getMinutes() * 60),
                format: 24
            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

    })

    .controller('TriggersController', function($scope, $state, $rootScope, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.trigger);
                    $rootScope.triggerTemp = $scope.temp;
                    $rootScope.triggerTime = $scope.time;
                    $state.go('app.setup'); 
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.selectTemp = function(val) {
            $scope.temp = val;
            console.debug(val);
        }

        $scope.selectTime = function(val) {
            $scope.time = val;
            console.debug(val);
        }

        $scope.temp = $rootScope.triggerTemp; 
        $scope.time = $rootScope.triggerTime; 
    })

    .controller('UnitsController', function($scope, $state, $rootScope, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.unit);
                    $rootScope.unit = $scope.unit;
                    $state.go('app.settings'); 
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.selectItem = function(val) {
            $scope.unit = val;
            console.debug(val);
        }

        $scope.units = [
            { name: "Celcius \u00B0C", val: 'c'},
            { name: "Fahrenheit \u00B0F", val: 'f'}
        ];
        $scope.unit = $rootScope.unit; 
    })

    .controller('BlocksController', function($scope, $state, $rootScope, $ionicPopup) {
        $scope.backButton = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Save changes',
                template: 'Do you want to save the changed settings?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log($scope.block);
                    $rootScope.block = $scope.block;
                    $state.go('app.setup');
                } else {
                    console.log('You are not sure');
                }
            });
        }

        $scope.selectItem = function(val) {
            $scope.block = val;
            console.debug(val);
        }

        $scope.blocks = [
            { name: '1 block', val: 1},
            { name: '10 blocks', val: 10}
        ];
        $scope.block = $rootScope.block; 
    })

    .controller('SystemController', function($scope, $state, $ionicPopup) {
        $scope.links = [
            { name: 'Calibration Report', url: 'app.calibration'},
            { name: 'System Details', url: 'app.details'},
            { name: 'Battery', url: 'app.battery'}
        ];

        $scope.probes = [1,2,3,4,5,6];
    })

    .controller('CalibrationController', function($scope, $stateParams) {
        $scope.result = {
            id: $stateParams.id,
            equipment: 'Calibrator ABC',
            quantity: 15,
            units: "Celcius",
            nominal: 120,
            actual: 115
        }
    });
