
angular.module('CurveX', ['ionic', 'CurveX.controllers', 'tqc-picker', 'ionic-datepicker', 'ionic-timepicker'])
    
    .run(function($ionicPlatform, $rootScope) {
        $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .config(function (ionicTimePickerProvider) {
        var timePickerObj = {
            inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
            format: 12,
            step: 15,
            setLabel: 'Set',
            closeLabel: 'Cancel'
        };
        ionicTimePickerProvider.configTimePicker(timePickerObj);
     })

    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'index.html',
                controller: 'AppController'
            })
            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'templates/dashboard.html',
                controller: 'DashboardController'
            })

            // Run
            .state('app.run', {
                url: '/run',
                templateUrl: 'templates/run/run.html',
                controller: 'StartController'
            })
            .state('app.record', {
                url: '/record',
                templateUrl: 'templates/run/record.html',
                controller: 'StartController'
            })
            .state('app.realtime', {
                url: '/realtime',
                templateUrl: 'templates/run/realtime.html',
                controller: 'RealtimeController'
            })
            .state('app.record-temp', {
                url: '/record-temp',
                templateUrl: 'templates/run/record-temp.html',
                controller: 'StartController'
            }) 
            .state('app.record-time', {
                url: '/record-time',
                templateUrl: 'templates/run/record-time.html',
                controller: 'StartTimeController'
            })

            // Run Setup
            .state('app.setup', {
                url: '/setup',
                templateUrl: 'templates/setup/setup.html',
                controller: 'SettingsController'
            })
            .state('app.paints', {
                url: '/paints',
                templateUrl: 'templates/setup/paints.html',
                controller: 'SettingsController'
            })
            .state('app.paint-create', {
                url: '/paint-create',
                templateUrl: 'templates/setup/paint-create.html',
                controller: 'CreatePaintController'
            })
            .state('app.paint-edit', {
                url: '/paints/:id',
                templateUrl: 'templates/setup/paint-edit.html',
                controller: 'EditPaintController'
            })
            .state('app.triggers', {
                url: '/triggers',
                templateUrl: 'templates/setup/triggers.html',
                controller: 'TriggersController'
            })
            .state('app.trigger-temp', {
                url: '/trigger-temp',
                templateUrl: 'templates/setup/trigger-temp.html',
                controller: 'TriggerTempController'
            })
            .state('app.trigger-time', {
                url: '/trigger-time',
                templateUrl: 'templates/setup/trigger-time.html',
                controller: 'TriggerTimeController'
            })
            .state('app.interval', {
                url: '/interval',
                templateUrl: 'templates/setup/interval.html',
                controller: 'IntervalController'
            })
            .state('app.blocks', {
                url: '/blocks',
                templateUrl: 'templates/setup/blocks.html',
                controller: 'BlocksController'
            })
  
            // Recordings
            .state('app.recordings', {
                url: '/recordings',
                templateUrl: 'templates/recordings/recordings.html',
                controller: 'RecordingsController'
            })
            .state('app.recordings-view', {
                url: '/recordings/:id',
                templateUrl: 'templates/recordings/recordings-view.html',
                controller: 'RecordingsViewController'
            })
            .state('app.view-graph', {
                url: '/recordings/:id/graph',
                templateUrl: 'templates/recordings/view-graph.html',
                controller: 'ViewGraphController'
            })

            // Instrument Setup
            .state('app.settings', {
                url: '/settings',
                templateUrl: 'templates/settings/settings.html',
                controller: 'SettingsController'
            })
            .state('app.language', {
                url: '/language',
                templateUrl: 'templates/settings/language.html',
                controller: 'LanguageController'
            })
            .state('app.units', {
                url: '/units',
                templateUrl: 'templates/settings/units.html',
                controller: 'UnitsController'
            })
            .state('app.date', {
                url: '/date',
                templateUrl: 'templates/settings/date.html',
                controller: 'DateTimeController'
            })
            .state('app.date-format', {
                url: '/date-format',
                templateUrl: 'templates/settings/date-format.html',
                controller: 'DateTimeController'
            })
            .state('app.time', {
                url: '/time',
                templateUrl: 'templates/settings/time.html',
                controller: 'DateTimeController'
            })
            .state('app.time-format', {
                url: '/time-format',
                templateUrl: 'templates/settings/time-format.html',
                controller: 'DateTimeController'
            })
            .state('app.display', {
                url: '/display',
                templateUrl: 'templates/settings/display.html',
                controller: 'DisplayController'
            })

            // System Information
            .state('app.system', {
                url: '/system',
                templateUrl: 'templates/settings/system.html',
                controller: 'SystemController'
            })
            .state('app.calibration', {
                url: '/calibration',
                templateUrl: 'templates/settings/system/calibration.html',
                controller: 'SystemController'
            })
            .state('app.calibration-probe', {
                url: '/calibration/:id',
                templateUrl: 'templates/settings/system/calibration-probe.html',
                controller: 'CalibrationController'
            })
            .state('app.details', {
                url: '/details',
                templateUrl: 'templates/settings/system/details.html',
                controller: 'SystemController'
            })
            .state('app.battery', {
                url: '/battery',
                templateUrl: 'templates/settings/system/battery.html',
                controller: 'SystemController'
            })
        $urlRouterProvider.otherwise('/app/dashboard');
    });
